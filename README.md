# GIFOnTerm

View gifs on the terminal

## Installation

Download the source code

```console
$ wget https://gitlab.com/Marcu5H/gifonterm/-/archive/master/gifonterm-master.tar.gz
$ tar -xf gifonterm.tar.gz && rm -f gifonterm.tar.gz && cd gifonterm
```

Install

```console
$ cargo install
```

## Usage

``` console
View gifs on the terminal

Usage: gifonterm [OPTIONS] [gif]

Arguments:
  [gif]

Options:
  -w, --width <width>    Display width of the gif
  -h, --height <height>  Display height of the gif
  -h, --help             Print help
  -V, --version          Print version
```
