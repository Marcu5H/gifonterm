use std::{
    fs::File,
    path::PathBuf,
    time::{Instant, Duration},
    sync::{atomic::AtomicBool, Arc},
    thread,
};
use image::{
    self,
    Delay,
    ImageBuffer,
    codecs::gif::GifDecoder, AnimationDecoder, ImageDecoder, Rgba, imageops,
};

pub enum ViewerError {
    FailedToOpenFile,
    MissingFile,
    FailedToDecodeGif,
    FileDoesNotExsits,
    NotDecoded,
}

struct CustomFrame {
    pub delay: Delay,
    pub buffer: ImageBuffer<Rgba<u8>, Vec<u8>>,
    pub x_offset: u32,
    pub y_offset: u32,
}

pub struct GifViewer {
    file_name: Option<PathBuf>,
    width: u32,
    height: u32,
    frames: Vec<CustomFrame>,
    keep_going: Arc<AtomicBool>,
}

impl GifViewer {
    pub fn new() -> Self {
        return Self {
            file_name: None,
            width: 0,
            height: 0,
            frames: Vec::new(),
            keep_going: Arc::new(AtomicBool::new(true)),
        };
    }

    pub fn set_width(&mut self, width: u32) {
        self.width = width;
    }

    pub fn set_height(&mut self, height: u32) {
        self.height = height;
    }

    pub fn file(&mut self, gif_file: PathBuf) -> Option<ViewerError> {
        if !gif_file.exists() {
            return Some(ViewerError::FileDoesNotExsits);
        }

        self.file_name = Some(gif_file);

        return None;
    }

    /// Decode and resize the gif
    pub fn decode(&mut self) -> Option<ViewerError> {
        // If we don't got any file, no need to continue.
        // Well, we can't.
        if self.file_name.is_none() {
            return Some(ViewerError::MissingFile);
        }

        // Open the file containing the gif
        let file = match File::open(self.file_name.clone().unwrap()) {
            Ok(f) => f,
            Err(_) => return Some(ViewerError::FailedToOpenFile),
        };

        // Attempt to decode the gif
        let decoder = match GifDecoder::new(file) {
            Ok(d) => d,
            Err(_) => return Some(ViewerError::FailedToDecodeGif),
        };

        if self.width == 0 && self.height == 0 {
            (self.width, self.height) = decoder.dimensions();
        }

        // Get the individual frames
        let frames = match decoder.into_frames().collect_frames() {
            Ok(f) => f,
            Err(_) => return Some(ViewerError::FailedToDecodeGif),
        };

        // Iterate over each frame
        for frame in frames {
            // Resize the image
            // TODO: Review other filters
            let image_buf = imageops::resize(frame.buffer(), self.width,
                                            self.height,
                                            imageops::FilterType::Nearest);

            // Save the image and frame metadata
            self.frames.push(CustomFrame {
                delay: frame.delay(),
                buffer: image_buf,
                x_offset: frame.left(),
                y_offset: frame.top()
            });
        }

        return None;
    }

    pub fn get_stop(&mut self) -> Arc<AtomicBool> {
        return self.keep_going.clone();
    }

    // Loop the gif and render each frame to the terminal
    pub fn view(&mut self) -> Option<ViewerError> {
        // Hide the cursor for a better view
        print!("\x1B[?25l");

        let mut q_flag = false;
        while !q_flag {
            for frame in &self.frames {
                if !self.keep_going.load(std::sync::atomic::Ordering::SeqCst) {
                    q_flag = true;
                    break;
                }

                let start_time = Instant::now();
                for h in frame.x_offset..self.height {
                    for w in frame.y_offset..self.width {
                        let colors = frame.buffer.get_pixel(w, h);
                        print!("\x1B[48;2;{};{};{}m \x1b[0m", colors[0], colors[1], colors[2]);
                    }
                    print!("\n");
                }
                print!("\x1B[{}F", self.height);

                let render_time =  Instant::now() - start_time;
                if render_time < Duration::from_millis(frame.delay.numer_denom_ms().0 as u64) {
                    thread::sleep(Duration::from_millis(frame.delay.numer_denom_ms().0 as u64) - render_time);
                }
            }
        }

        // Make the cursor visible again
        print!("\x1B[?25h");

        return None;
    }
}
