use std::process::exit;

use clap::{Arg, Command, ArgAction};
use gifonterm::viewer::GifViewer;
use ctrlc;

fn main() {
    let matches = Command::new("gifonterm")
        .about("View gifs on the terminal")
        .version("0.1.0")
        .arg_required_else_help(true)
        .arg(
            Arg::new("gif")
                .action(ArgAction::Set),
        )
        .arg(
            Arg::new("width")
                .short('w')
                .long("width")
                .help("Display width of the gif")
                .value_parser(clap::value_parser!(u32))
                .action(ArgAction::Set),
        )
        .arg(
            Arg::new("height")
                .short('h')
                .long("height")
                .help("Display height of the gif")
                .value_parser(clap::value_parser!(u32))
                .action(ArgAction::Set),
        )
        .get_matches();

    let file: &String = matches.get_one::<String>("gif").unwrap();

    let mut view = GifViewer::new();
    if let Some(_e) = view.file(std::path::PathBuf::from(&file)) {
        println!("File `{}` does not exist!", file);
        exit(1);
    }

    if matches.contains_id("width") {
        view.set_width(*matches.get_one::<u32>("width").unwrap())
    }
    if matches.contains_id("height") {
        view.set_height(*matches.get_one::<u32>("height").unwrap())
    }

    let stop = view.get_stop();

    ctrlc::set_handler(move || {
        stop.store(false, std::sync::atomic::Ordering::SeqCst);
    }).expect("Error setting Ctrl-C handler");

    if let Some(_e) = view.decode() {
        println!("Failed to decode GIF!");
        exit(1);
    }

    if let Some(_e) = view.view() {
        println!("Failed to view GIF!");
        exit(1);
    }
}
